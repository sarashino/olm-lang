extern crate pest;

use pest::error::Error;
use pest::iterators::Pairs;
use pest::Parser;
use pest_derive::Parser;

#[derive(Parser)]
#[grammar = "parser.pest"]
struct OlmParser;

pub fn parse(source: &str) -> Result<Pairs<'_, Rule>, Error<Rule>> {
    OlmParser::parse(Rule::ground, source)
}
pub fn test() {
    let pairs = OlmParser::parse(
        Rule::ident_list,
        "a1 b2 a1' 0x123 0x0 0B101 0123 1,234 123,000.000 00 TODO abc
        test::test
        test%%test
        0xff.f
        0xff.fp3
        0xff.fe10
        123,456.789e10
        0B1010e10
        'symbol a' a''' \"\\
        | text dayoac
        | test
        | test \
daou\"
\"\"\"
tesuto
dayo
\\circ
\"\"\"
        2022-08-07 10:00 10:00Z 10:00+09:00
        1990-10-28T10:00+09:00
        prelude::println
        tag||tag`test`
        `\\
\t| test\\
\t| dayo\\
` \
        `back {{tesut}} quoted`",
    )
    .unwrap_or_else(|e| panic!("{}", e));

    // Because ident_list is silent, the iterator will contain idents
    for pair in pairs {
        // A pair is a combination of the rule which matched and a span of input
        println!("Rule:    {:?}", pair.as_rule());
        println!("Span:    {:?}", pair.as_span());
        println!("Text:    {}", pair.as_str());

        // A pair can be converted to an iterator of the tokens which make it up:
        for inner_pair in pair.into_inner() {
            match inner_pair.as_rule() {
                Rule::alphabet => println!("Letter:  {}", inner_pair.as_str()),
                Rule::digit => println!("Digit:   {}", inner_pair.as_str()),
                Rule::template_literal => {
                    println!("template_literal:   {:?}", inner_pair.into_inner())
                }
                rule => println!("{:?}:   {}", rule, inner_pair.as_str()),
            };
        }
    }
    let pairs2 = OlmParser::parse(
        Rule::ground,
        "
fun = (fig)
fun = fig -}}- test
fun a = reader:%macro(fig a (fig2 test \"dayo\"))  -- test
  {-
  |# tesut 
  |dayo
  -}
fun a b = -- test
  (fig a -- test
    (fig2 test \"dayo\"))
cfun = a . b . \"test\"

ident : Test a; Test b; testuto
ident = 123,345.01

ident2: Test a b; tes  // test
  t dayo ( {-
  test
  -}
test::snth
  )
\"\"\"
test
\"\"\"
ident2 a b = `\\
{{a . b}}
test
`
  where
    test = 123
    test = tag`test`

[ : ]l1 = 123,345.01
[ -}}-] ]r2 _ = 123,345.01
{ %$-- } _ _ list = 123,345.01
{ }-- } = etst --- tset
{ -{-- } = ets---tset
[ --- ]r1 = etst{--test
{ {-- } = ets---tset
{ --} } = ets---tset

+ = ets---tset
+-- = ets---tset
--- test = (--- test)
",
    )
    .unwrap_or_else(|e| panic!("{}", e));

    // Because ident_list is silent, the iterator will contain idents
    for pair in pairs2 {
        println!("\n\nRule:    {:?}", pair.as_rule());
        println!("Span:    {:?}", pair.as_span());
        println!("Text:    {}", pair.as_str());

        for inner_pair in pair.into_inner() {
            match inner_pair.as_rule() {
                rule => println!("{:?}:   {:?}\n", rule, inner_pair.into_inner()),
            };
        }
    }
}
