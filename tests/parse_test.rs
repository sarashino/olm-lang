use std::fs;
extern crate olm_lang;

#[test]
fn parse_test() {
    let file_path = "tests/case/case1/main.olm";
    let file = fs::read_to_string(file_path).unwrap();
    let result = olm_lang::parser::parse(&file);
    assert!(result.is_ok(), "{}", result.unwrap_err());
}
